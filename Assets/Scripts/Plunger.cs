﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Plunger : MonoBehaviour {
    float power;
    float minPower = 0f;
    public float maxPower;
    List<Rigidbody> ballList;
    bool ballReady;

	// Use this for initialization
	void Start ()
    {
        ballList = new List<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (ballList.Count > 0)
        {
            ballReady = true;
            if (Input.GetKey(KeyCode.Space))
            {
                if (power <= maxPower)
                {
                    power += 50 * Time.deltaTime;
                }
            }
            if (Input.GetKeyUp(KeyCode.Space))
            {
                foreach(Rigidbody r in ballList)
                {
                    r.AddForce(power*Vector3.forward);
                }
            }
        }
        else
        {
            ballReady = false;
            power = 0f;
        }
	}
    private void OnTriggerEnter (Collider other)
    {
        if (other.gameObject.CompareTag("Ball"))
        {
            ballList.Add(other.gameObject.GetComponent<Rigidbody>());
        }
    }
    private void OnTriggerExit (Collider other)
    {
        if (other.gameObject.CompareTag("Ball"))
        {
            ballList.Remove(other.gameObject.GetComponent<Rigidbody>());
            power = 0f;
        }
    }
}
