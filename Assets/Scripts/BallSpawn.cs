﻿using UnityEngine;
using System.Collections;

public class BallSpawn : MonoBehaviour {
    private Vector3 startPos;
    bool death;

	// Use this for initialization
	void Start ()
    {
        startPos = this.transform.position;
        death = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if (death)
        {
            Restart();
        }
	}

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Death")
        {
            death = true;
        }
    }

    void Restart()
    {
        this.transform.position = startPos;
        death = false;
    }
}
